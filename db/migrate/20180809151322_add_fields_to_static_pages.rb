class AddFieldsToStaticPages < ActiveRecord::Migration[5.2]
  def change
    add_column :static_pages, :latitude, :float
    add_column :static_pages, :longitude, :float
  end
end
