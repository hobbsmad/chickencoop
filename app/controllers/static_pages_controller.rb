class StaticPagesController < ApplicationController
  def index
  end

  def coopkearney
    @skip_footer = true
  end

  def urbandalecoop
    @skip_footer = true
  end

  def coopwestdesmoines
    @skip_footer = true
  end

  def coopgrandisland
    @skip_footer = true
  end
end
