Rails.application.routes.draw do
  root 'static_pages#index'
  get "/pages/:page" => "pages#show"

  get '/static_pages/coopkearney', to: 'static_pages#coopkearney', as: :coopkearney
  get '/static_pages/urbandalecoop', to: 'static_pages#urbandalecoop', as: :urbandalecoop
  get '/static_pages/coopgrandisland', to: 'static_pages#coopgrandisland', as: :coopgrandisland
  get '/static_pages/about', to: 'static_pages#about', as: :about
  get '/static_pages/sports', to: 'static_pages#sports', as: :sports
  get '/static_pages/menu', to: 'static_pages#menu', as: :menu
  get '/static_pages/specials', to: 'static_pages#specials', as: :specials
  get '/static_pages/contactus', to: 'static_pages#contactus', as: :contactus
  get '/static_pages/coopgrandislandmenu', to: 'static_pages#coopgrandislandmenu', as: :coopgrandislandmenu
  get '/static_pages/coopgrandislandcontactus', to: 'static_pages#coopgrandislandcontactus', as: :coopgrandislandcontactus
  get '/static_pages/urbandalecoopmenu', to: 'static_pages#urbandalecoopmenu', as: :urbandalecoopmenu
  get '/static_pages/contacturbandale', to: 'static_pages#contacturbandale', as: :contacturbandale
  get '/static_pages/coopwestdesmoines', to: 'static_pages#coopwestdesmoines', as: :coopwestdesmoines
  get '/static_pages/westcontactus', to: 'static_pages#westcontactus', as: :westcontactus
  get '/static_pages/westmenu', to: 'static_pages#westmenu', as: :westmenu
  get '/static_pages/join', to: 'static_pages#join', as: :join
end
